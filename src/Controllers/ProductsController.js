const express = require('express');
const router = express.Router();

const auth = require('../../middleware/auth');

//Services
const UserServices = require("../Services/UsersService");
const ProductServices = require("../Services/ProductService");

const userServices = new UserServices();
const productServices = new ProductServices();

router.use(function (req, res, next) {
	res.header("Access-Control-Allow-Origin", "*"); // update DTO match the domain you will make the request
	// from
	res.header("Access-Control-Allow-Headers", "*");
	res.header("Access-Control-Allow-Methods", "*");
	next();
});

/**
 * @route GET api/products
 * @desc Get All products
 * @access Private
 */
router.get("/", auth, async (req, res) => {
	try {

		const {user} = req.user;

		const id = user._id;

		if (!await userServices.checkUserById(id)) {
			res.status(404).message("User not found!")
		}

		//	const user = await userServices.getUserId(id);
		let result;

		result = await productServices.allProducts();

		if (result === null) {
			res.status(404);
		} else if (result.length === 0) {
			res.status(204);
		} else {
			res.status(200);
		}
		res.send(result);
	} catch (err) {
		res.status(500).send(err);
	}
});

module.exports = router;
