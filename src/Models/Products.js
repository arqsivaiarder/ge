const mongoose = require("mongoose");
const Schema = mongoose.Schema;

//Create Schema
const ProductSchema = new Schema({
	user: {
		type: Schema.Types.ObjectId,
		ref: "users"
	},
	product: {
		type: String,
		required: true
	}
});

module.exports = Products = mongoose.model("products", ProductSchema);
