const verifier = require('../Mapper/Utils');
const ProductsRepository = require('../Repository/ProductsRepository');

const repo = new ProductsRepository();

class ProductService {

	async allProducts() {
		const orders = await repo.Orders().find();

		let map = new Map();

		let i, order, product;

		for (i = 0; i < orders.length; i++) {
			order = orders[i];
			product = order.product;
			// console.log(product)
			if (!map.has(product)) {
				map.set(product, 1);
			} else {
				for (let m in map) {
					if (m === product) {
						let productVal = map.get(product);
						productVal++;

						map.get(product).set(productVal);
					}
				}
			}
		}
		let ret = [];
		for (let [k, v] of map) {
			let pid = k;
			let val = v;

			ret.push({product: pid, count: val})
		}
		console.log(ret);
		return ret;
	}
}

module.exports = ProductService;
